package config

// Configuration is the struct for configs
type Configuration struct {
	Var *VariableConfigs
}

type VariableConfigs struct {
	MainExchangeName       string `default:"rabbitDlqMainEx"`
	MainQueueName          string `default:"rabbitDlqMainQ"`
	DeadLetterExchangeName string `default:"rabbitDlqDlx"`
}

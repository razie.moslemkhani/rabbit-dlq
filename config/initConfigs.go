package config

import (
	"fmt"
	"github.com/spf13/viper"
)

var (
	Config *Configuration
)

func InitConfigurations() {
	loadViper()
	var c = new(Configuration)
	c.Var = new(VariableConfigs)
	err := viper.UnmarshalKey("variableConfigs", c.Var)
	if err != nil {
		panic(err)
	}
	Config = c
}

func loadViper() {
	configFilePath := "config.yaml"
	viper.SetConfigFile(configFilePath)
	if err := viper.ReadInConfig(); err != nil {
		panic(fmt.Errorf("Config file %s not found.\n%v", configFilePath, err))
	}
}

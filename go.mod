module RabbitDelayedQueue

go 1.13

require (
	github.com/spf13/viper v1.7.1 // indirect
	github.com/streadway/amqp v1.0.0
)

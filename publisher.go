package rabbitMQDelayedQueue

import (
	"RabbitDelayedQueue/config"
	"fmt"
	"github.com/streadway/amqp"
	"time"
)

type RabbitMQConfig struct {
	Config     *Configuration
	connection *amqp.Connection
	channel    *amqp.Channel
	exchange   string
}

type Configuration struct {
	User     string
	Password string
	Host     string
	Port     string
}

func (r *RabbitMQConfig) Connect() error {
	config.InitConfigurations()
	rabbitAddr := r.Config.generateAddr()
	fmt.Printf("rabbitmq address is: %s", rabbitAddr)

	connection, err := amqp.Dial(rabbitAddr)
	if err != nil {
		return err
	}
	r.connection = connection
	return nil
}

func (r *RabbitMQConfig) Publish(msg string, queueName string, ttl time.Duration) error {
	err := r.createChannel()
	if err != nil {
		return err
	}

	err = r.createExchanges()
	if err != nil {
		return err
	}

	err = r.createQueues(queueName, ttl)
	if err != nil {
		return err
	}

	// We create a message to be sent to the queue
	message := amqp.Publishing{
		Body: []byte(msg),
	}

	// We publish the message to the exchange we created earlier
	err = r.channel.Publish(r.exchange, "random-key", false, false, message)
	if err != nil {
		fmt.Println("error publishing a message to the queue:" + err.Error())
		return err
	}
	return nil
}

func (r *RabbitMQConfig) createExchanges() error {
	err := r.channel.ExchangeDeclare(
		config.Config.Var.MainExchangeName,
		amqp.ExchangeDirect,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		fmt.Printf("failed to create the exchange, %s", err.Error())
		return err
	}

	err = r.channel.ExchangeDeclare(
		config.Config.Var.DeadLetterExchangeName,
		amqp.ExchangeDirect,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		fmt.Printf("failed to create the exchange, %s", err.Error())
		return err
	}

	fmt.Println("exchanges declared")
	return nil
}

func (r *RabbitMQConfig) createQueues(queueName string, ttl time.Duration) error {
	//dlx queue and binding
	_, err := r.channel.QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		fmt.Println("error declaring the queue: " + err.Error())
		return err
	}

	err = r.channel.QueueBind(queueName, "random-key", config.Config.Var.DeadLetterExchangeName, false, nil)
	if err != nil {
		fmt.Println("error binding to the queue: " + err.Error())
		return err
	}

	args := make(amqp.Table)
	args["x-message-ttl"] = int32(ttl)
	args["x-dead-letter-exchange"] = config.Config.Var.DeadLetterExchangeName
	args["x-dead-letter-routing-key"] = "random-key"
	_, err = r.channel.QueueDeclare(config.Config.Var.MainQueueName, true, false, false, false, args)
	if err != nil {
		fmt.Println("error declaring the queue: " + err.Error())
		return err
	}

	// We bind the queue to the exchange to send and receive data from the queue
	err = r.channel.QueueBind(config.Config.Var.MainQueueName, "random-key", config.Config.Var.MainExchangeName, false, nil)
	if err != nil {
		fmt.Println("error binding to the queue: " + err.Error())
		return err
	}

	return nil
}

func (r *RabbitMQConfig) createChannel() error {
	channel, err := r.connection.Channel()
	if err != nil {
		return err
	}
	r.channel = channel
	return nil
}

func (c *Configuration) generateAddr() string {
	return "amqp://" + c.User + ":" + c.Password + "@" + c.Host + ":" + c.Port
}
